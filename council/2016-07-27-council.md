# 2016-07-27 Council Meeting

Logs: http://logs.xmpp.org/council/2016-07-27#15:15:09


## Roll call

- Lance (chairing)
- Tobias
- psa
- MattJ
- Dave Cridland (busy)


## Old Business

None


## Agenda

- Note that there is now a Stack Overflow project to document XMPP which might
  be of interest to members of the community:
  http://stackoverflow.com/documentation/xmpp

## Date of next

2016-08-03 15:00:00 UTC


## AOB

- Editor needs to merge changes to XEP-0300 to prepare it for Last Call.


EOM
