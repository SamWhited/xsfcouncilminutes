# 2016-02-17 Council Meeting

Logs: http://logs.xmpp.org/council/2016-02-17/#16:04:16


## Roll call

- Dave Cridland (chairing)
- Tobias
- MattJ
- psa


## Instant Stream Resumption: Accept as experimental?

https://xmpp.org/extensions/inbox/isr.html

- Dave Cridland: On list
- MattJ: On list
- Tobias: -1
- psa: On list


## AOB

None


## Date of next

sbtsbc


EOM
