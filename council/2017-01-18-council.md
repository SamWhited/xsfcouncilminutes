# 2017-01-18 Council Meeting

Logs: http://logs.xmpp.org/council/2017-01-18#16:01:18


## Roll call

- Tobias (chairing)
- Sam Whited
- Dave Cridland

Daniel and Link Mauve sent their apologies.


## Move XEP-0153: vCard-Based Avatars to "Obsolete"

https://github.com/xsf/xeps/pull/357

- Tobias         -1
- SamWhited      +1
- Dave Cridland  -1

Others to vote on list if they want too.

## Specify an encoding in XEP-0300

https://github.com/xsf/xeps/issues/349

Current proposal is to specify an encoding and bump Jingle File Transfer and
Hashes namespace version.
After some discussion on whether a Jingle FT version bump was required, it was
determined that it was since hashes are REQUIRED when offering a file in FT.


## Move 2016 Compliance Suites to LC

- Tobias notes that it probably makes sense just to start new ones for 2017 at
  this point.
- Sam to start a list discussion on 2017 suites.
- Sam suggests deprecating the 2010 ones (or whatever the currently "accepted"
  ones are) to prevent old recommendations from being implemented and for the
  image (looks bad that the only recommendations are so old).
- Tobias suggests that we need a new one first.

## Accept Bind 2.0 as Experimental

https://xmpp.org/extensions/inbox/bind2.0.html

- SamWhited      +1
- Dave Cridland  +1
- Tobias         +1

Others on list.

## Lots of deferred XEPs

https://github.com/xsf/xeps/pull/371

Note that Sam (with his editor hat on) recently deferred a bunch of XEPs that
weren't automatically deferred after 12 months (but should have been).

If you see anything that needs to be advanced instead, please reach out to the
council.

## Consider advancing XEP-0333: Chat Markers to LC

- This is one of the XEPs that Sam deferred, but he thinks its widely used so
  maybe it's worth advancing.
- Tobias, Dev, and Dave question the assumption that it's widely used.
- Dave notes that this is what LC is for, if it's good enough to implement, and
  seems to be working, it should be LCed and people will respond if there are
  issues with it.
- Dave also notes that "widespread use" might matter for Draft->Final but not
  for Experimental->Draft

## Consider advancing XEP-0233: XMPP Server Registration for use with Kerberos V5

- Dave Cridland notes that Mili should probably be an author because she wrote
  a great deal of that one and that he's happy to LC it as well
- Tobias also is happy to LC this one

## Date of next

2016-01-25 1615Z

## Any Other Business

None

EOM
